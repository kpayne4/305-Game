.global delay

delay:
    and r1, r1, #0
.for:
    add r1, r1, #1    /* increment*/ 
    cmp r1, r0        /* less than check*/
    ble .for
    beq .done
.done:
    mov pc, lr
